import datetime
import json
import logging.config
import os
import time
from threading import Thread
import yaml
from pykafka import KafkaClient
from pykafka.common import OffsetType
from sqlalchemy import create_engine, and_
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import sqlite3
import connexion
from usage_data_events import UsageDataEvents
from equipment_status_events import EquipmentStatusEvents
from anomalies import Anomaly
from flask_cors import CORS
Base = declarative_base()

if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    APP_CONF_FILE = "/config/app_conf.yml"
    LOG_CONF_FILE = "/config/log_conf.yml"
else:
    APP_CONF_FILE = "app_conf.yml"
    LOG_CONF_FILE = "log_conf.yml"

if "TARGET_ENV" not in os.environ or os.environ["TARGET_ENV"] != "test":
    CORS(app.app)
    app.app.config['CORS_HEADERS']='Content-Type'

with open(APP_CONF_FILE, 'r') as f:
    app_config = yaml.safe_load(f.read())

with open(LOG_CONF_FILE, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info(f"App Conf File: {APP_CONF_FILE}")
logger.info(f"Log Conf File: {LOG_CONF_FILE}")

def create_db():
    conn = sqlite3.connect(app_config['datastore']['filename'])

    c = conn.cursor()

    c.execute('''
            CREATE TABLE anomaly
            (id INTEGER PRIMARY KEY ASC, 
            event_id VARCHAR(250) NOT NULL,
            trace_id VARCHAR(250) NOT NULL,
            event_type VARCHAR(100) NOT NULL,
            anomaly_type VARCHAR(100) NOT NULL,
            description VARCHAR(250) NOT NULL,
            date_created VARCHAR(100) NOT NULL)
            ''')

    conn.commit()
    conn.close()

DB_ENGINE = create_engine("sqlite:///%s"%app_config['datastore']['filename'])
Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)

if not os.path.isfile(app_config['datastore']['filename']):
    logger.info("PATH DOES NOT EXIST")
    create_db()

def get_anomalies():
    """ Process event messages for anomaly detection """
    hostname = "%s:%d" % (
        app_config["events"]["hostname"], app_config["events"]["port"])

    max_retries = app_config["startup"]["max_retries"]
    retry_count = 0

    while retry_count < max_retries:
        try:
            client = KafkaClient(hosts=hostname)
            topic = client.topics[str.encode(app_config["events"]["topic"])]
            logger.info("Connected Successfully to Kafka")

            break
        except Exception as e:
            logger.error(f"Connection Failed: {e}")
            time.sleep(app_config["startup"]["sleep"])
            retry_count += 1

    consumer = topic.get_simple_consumer(consumer_group=b'anomaly_detector_group',
                                         reset_offset_on_start=False,
                                         auto_offset_reset=OffsetType.LATEST)

    for msg in consumer:
        msg_str = msg.value.decode('utf-8')
        msg = json.loads(msg_str)
        logger.info(f"Message: {msg}")

        payload = msg["payload"]
        event_type = msg["type"]
        trace_id = payload["trace_id"]
        event_id = payload["uuid"]

        if event_type == "usage_data_events":
            shuttle_speed = payload["shuttle_speed"]
            if  shuttle_speed > app_config["anomalies"]["shuttle_speed_threshold"]:
                record_anomaly(event_id, trace_id, event_type, "High Shuttle Speed", payload["shuttle_speed"])
        elif event_type == "equipment_status_events":
            wear_level = payload["wear_level"]
            if wear_level > app_config["anomalies"]["wear_level_threshold"]:
                record_anomaly(event_id, trace_id, event_type, "High Wear Level", payload["wear_level"])

        consumer.commit_offsets()
        logger.info("Message processed and offset committed")

def record_anomaly(event_id, trace_id, event_type, anomaly_type, value):
    """ Records an anomaly to the database """
    session = DB_SESSION()
    description = f"{event_type} of {value} is over threshold"
    anomaly = Anomaly(event_id, trace_id, event_type, anomaly_type, description)
    session.add(anomaly)
    session.commit()
    logger.info(f"Anomaly recorded: {description}")
    session.close()

app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi1.yaml",
            base_path="/anomalies",
            strict_validation=True,
            validate_responses=True)

if __name__ == "__main__":
    app.run(port=8120)