import connexion
from connexion import NoContent
import time
import json
from datetime import datetime as dt, timedelta
from sqlalchemy import create_engine, desc
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import yaml
import logging
import logging.config
from apscheduler.schedulers.background import BackgroundScheduler
import sqlite3
import requests
from stats import Stats
from pytz import utc
from flask_cors import CORS
import os
from pykafka import KafkaClient
Base = declarative_base()

if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yml"
    log_conf_file = "log_conf.yml"

if "TARGET_ENV" not in os.environ or os.environ["TARGET_ENV"] != "test":
    CORS(app.app)
    app.app.config['CORS_HEADERS']='Content-Type'

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

# External Logging Configuration
with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

def create_db():
    conn = sqlite3.connect(app_config['datastore']['filename'])

    c = conn.cursor()

    c.execute('''
            CREATE TABLE stats
            (id INTEGER PRIMARY KEY ASC,
            num_usage_data_events INTEGER NOT NULL,
            num_equipment_status_events INTEGER NOT NULL,
            max_shuttle_speed INTEGER NOT NULL,
            max_wear_level INTEGER NOT NULL,
            last_updated VARCHAR(100) NOT NULL)
            ''')

    c.execute('''
        INSERT INTO stats (num_usage_data_events, num_equipment_status_events, max_shuttle_speed, max_wear_level, last_updated)
        VALUES (0, 0, 0, 0, datetime('now'))
    ''')

    conn.commit()
    conn.close()

if not os.path.isfile(app_config['datastore']['filename']):
    logger.info("PATH DOES NOT EXIST")
    create_db()

if os.path.isfile(app_config['datastore']['filename']):
    logger.info("PATH DOES EXIST")
    logger.info("ABSOLUTE PATH: " + os.path.abspath(app_config['datastore']['filename']))


DB_ENGINE = create_engine("sqlite:///%s"%app_config['datastore']['filename'])
Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)

max_retries = app_config["startup"]["max_retries"]
retry_count = 0

while retry_count < max_retries:
    try:
        hostname = app_config['events']['hostname']
        configuration = app_config['events']['port']
        client = KafkaClient(hosts=f"{hostname}:{configuration}")
        event_log_topic = client.topics[str.encode(app_config["events"]["topic2"])]
        event_log_producer = event_log_topic.get_sync_producer()

        startup_msg = {
            "type": "processing_startup",
            "datetime": dt.now().strftime("%Y-%m-%dT%H:%M:%S"),
            "payload": {
                "message": "Processing service has started and connected to Kafka",
                "code": "0003"
            }
        }

        event_log_producer.produce(json.dumps(startup_msg).encode('utf-8'))
        logger.info("Startup message published to event_log topic")

        break
    except Exception as e:
        logger.error(f"Connection Failed: {e}")
        time.sleep(app_config["startup"]["sleep"])
        retry_count += 1

def get_stats():
    """ Gets event stats"""
    try:
        logger.info("GET request for /events/stats started")

        session = DB_SESSION()

        results = session.query(Stats).order_by(desc(Stats.last_updated)).first()
        session.close()

        if results is None:
            logger.error("Statistics do not exist")
            return {"message": "Statistics do not exist"}, 404

        results_dict = {
            "num_usage_data_events": results.num_usage_data_events,
            "num_equipment_status_events": results.num_equipment_status_events,
            "max_shuttle_speed": results.max_shuttle_speed,
            "max_wear_level": results.max_wear_level,
            "last_updated": dt.now()
        }

        logger.info("GET request for /events/stats completed")

        return results_dict, 200
    except Exception as e:
        logger.error(e)
        return NoContent, 500

def populate_stats():
    """ Periodically update stats """
    logger.info("Periodic Processing Started")
    date_time = dt.now()
    logger.info("NEW LOG MESSAGE......................")
    session = DB_SESSION()
    current_stats = session.query(Stats).order_by(desc(Stats.last_updated)).first()
    if not current_stats: # or not (current_stats.num_equipment_status_events and current_stats.num_usage_data_events):
        logger.info("using default values")
        current_stats = Stats(num_usage_data_events= 0,
                                num_equipment_status_events= 0,
                                max_shuttle_speed= 0,
                                max_wear_level= 0,
                                last_updated= dt.now() - timedelta(days=1))
        session.add(current_stats)
        session.commit()

    start = current_stats.last_updated
    start = start.strftime("%Y-%m-%dT%H:%M:%S")
    end = date_time.strftime("%Y-%m-%dT%H:%M:%S")

    get_usage_data = requests.get(f'{app_config["eventstore"]["url"]}/usage_data_events_get', params={'start_timestamp': start, 'end_timestamp': end})
    get_equipment_data = requests.get(f'{app_config["eventstore"]["url"]}/equipment_status_events_get', params={'start_timestamp': start, 'end_timestamp': end})

    if (get_usage_data.status_code == 200) and (get_equipment_data.status_code == 200):
        logger.info(f"Total usage data events received: {len(get_usage_data.json())}")
        logger.info(f"Total equipment status events received: {len(get_equipment_data.json())}")
        logger.info(f"Total events received: {len(get_equipment_data.json()) + len(get_usage_data.json())}")

        total_events = len(get_equipment_data.json()) + len(get_usage_data.json())
        if total_events > app_config['events']['limit']:
            limit_reached = {
                "type": "limit_reached",
                "datetime": dt.now().strftime("%Y-%m-%dT%H:%M:%S"),
                "payload": {
                    "message": "Periodic processing has received more than a configurable number of messages (25)",
                    "code": "0004"
                }
            }
            event_log_producer.produce(json.dumps(startup_msg).encode('utf-8'))
            logger.info("Periodic processing has received too many messages. More info published to event_log topic")

        num_usage_data_events = current_stats.num_usage_data_events + len(get_usage_data.json())
        num_equipment_status_events = current_stats.num_equipment_status_events + len(get_equipment_data.json())

        usage_data_events = get_usage_data.json()
        if usage_data_events:
            for event in usage_data_events:
                if event['shuttle_speed'] > current_stats.max_shuttle_speed:
                    max_shuttle_speed = event['shuttle_speed']
                else:
                    max_shuttle_speed = current_stats.max_shuttle_speed
                trace_id = event['trace_id']
                logger.debug(f"Processed usage data event with trace_id {trace_id}")
        else:
            max_shuttle_speed = current_stats.max_shuttle_speed
            logger.info("usage_data_events is empty, max_shuttle_speed is unchanged")

        equipment_status_events = get_equipment_data.json()
        if equipment_status_events:
            for event in equipment_status_events:
                if event['wear_level'] > current_stats.max_wear_level:
                    max_wear_level = event['wear_level']
                else:
                    max_wear_level = current_stats.max_wear_level
                trace_id = event['trace_id']
                logger.debug(f"Processed equipment status event with trace_id {trace_id}")
        else:
            max_wear_level = current_stats.max_wear_level
            logger.info("usage_data_events is empty, max_wear_level is unchanged")


        new_stats = Stats(num_usage_data_events= num_usage_data_events,
                          num_equipment_status_events= num_equipment_status_events,
                          max_shuttle_speed= max_shuttle_speed,
                          max_wear_level= max_wear_level,
                          last_updated= date_time)
                          
        session.add(new_stats)

    else:
        logger.error("Did not receive 200 response code")

    session.commit()

    session.close()
    logger.info("Periodic Processing Ended")

def init_scheduler():
    sched = BackgroundScheduler(daemon=True, timezone=utc)
    sched.add_job(populate_stats,
                'interval',
                seconds=app_config['scheduler']['period_sec'])
    sched.start()

app = connexion.FlaskApp(__name__, specification_dir='')

app.add_api("openapi.yml",
            base_path="/processing",
            strict_validation=True,
            validate_responses=True)

if __name__ == "__main__":
    # run our standalone event server
    init_scheduler()
    app.run(port=8100)
