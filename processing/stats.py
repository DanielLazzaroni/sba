from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

class Stats(Base):
    """ Processing Statistics """
    __tablename__ = "stats"
    id = Column(Integer, primary_key=True)
    num_usage_data_events = Column(Integer, nullable=False)
    num_equipment_status_events = Column(Integer, nullable=False)
    max_shuttle_speed = Column(Integer, nullable=False)
    max_wear_level = Column(Integer, nullable=False)
    last_updated = Column(DateTime, nullable=False)
    
    def __init__(self, num_usage_data_events, num_equipment_status_events, max_shuttle_speed, max_wear_level, last_updated):
        """ Initializes a processing statistics objet """
        self.num_usage_data_events = num_usage_data_events
        self.num_equipment_status_events = num_equipment_status_events
        self.max_shuttle_speed = max_shuttle_speed
        self.max_wear_level = max_wear_level
        self.last_updated = last_updated
        
    def to_dict(self):
        """ Dictionary Representation of a statistics """
        dict = {}
        dict['num_usage_data_events'] = self.num_usage_data_events
        dict['num_equipment_status_events'] = self.num_equipment_status_events
        dict['max_shuttle_speed'] = self.max_shuttle_speed
        dict['max_wear_level'] = self.max_wear_level
        dict['last_updated'] = self.last_updated.strftime("%Y-%m-%dT%H:%M:%S")
        return dict