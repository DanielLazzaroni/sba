import sqlite3

conn = sqlite3.connect('stats.sqlite')

c = conn.cursor()

c.execute('''
          CREATE TABLE stats
          (id INTEGER PRIMARY KEY ASC,
           num_usage_data_events INTEGER NOT NULL,
           num_equipment_status_events INTEGER NOT NULL,
           max_shuttle_speed INTEGER NOT NULL,
           max_wear_level INTEGER NOT NULL,
           last_updated VARCHAR(100) NOT NULL)
          ''')

conn.commit()
conn.close()

