import mysql.connector

db_conn = mysql.connector.connect(
    host="lazzaroni-cloud.eastus.cloudapp.azure.com",
    user="user",
    password="password",
    database="events")

db_cursor = db_conn.cursor()

db_cursor.execute('''
    DROP TABLE usage_data_events, equipment_status_events
    ''')

db_conn.commit()
db_conn.close()
