import datetime
import os
import time
from threading import Thread
import json
from operator import and_
import logging.config
import logging
import yaml
from usage_data_events import UsageDataEvents
from equipment_status_events import EquipmentStatusEvents
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from pykafka.common import OffsetType
from pykafka import KafkaClient
import connexion
Base = declarative_base()

if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    APP_CONF_FILE = "/config/app_conf.yml"
    LOG_CONF_FILE = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    APP_CONF_FILE = "app_conf.yml"
    LOG_CONF_FILE = "log_conf.yml"

with open(APP_CONF_FILE, 'r') as f:
    app_config = yaml.safe_load(f.read())

# External Logging Configuration
with open(LOG_CONF_FILE, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info(f"App Conf File: {APP_CONF_FILE}")
logger.info(f"Log Conf File: {LOG_CONF_FILE}")

user = app_config["datastore"]["user"]
password = app_config["datastore"]["password"]
hostname = app_config["datastore"]["hostname"]
port = app_config["datastore"]["port"]
db = app_config["datastore"]["db"]

DB_ENGINE = create_engine(
    f'mysql+pymysql://{user}:{password}@{hostname}:{port}/{db}',
    pool_pre_ping=True,
    pool_recycle=240
)
Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)
logger.info(f'Connecting to DB. Hostname: {hostname}, Port: {port}')


def get_usage_data_events(start_timestamp, end_timestamp):
    """ Gets new usage data event readings between the start and end timestamps """
    try:
        session = DB_SESSION()

        start_timestamp_datetime = datetime.datetime.strptime(
            start_timestamp, "%Y-%m-%dT%H:%M:%S")
        end_timestamp_datetime = datetime.datetime.strptime(
            end_timestamp, "%Y-%m-%dT%H:%M:%S")

        results = session.query(UsageDataEvents).filter(and_(UsageDataEvents.date_created >=
                                                             start_timestamp_datetime,
                                                             UsageDataEvents.date_created < end_timestamp_datetime))

        results_list = []

        for event in results:
            results_list.append(event.to_dict())

        session.close()

        logger.info(f"""Query for Usage Data readings after {start_timestamp} and
                    before {end_timestamp} returns {len(results_list)} results""")  # runs
        return results_list, 200
    except Exception as e:
        logger.error(e)


def get_equipment_status_events(start_timestamp, end_timestamp):
    """ Gets new equipment status event readings between the start and end timestamps """
    try:
        session = DB_SESSION()

        start_timestamp_datetime = datetime.datetime.strptime(
            start_timestamp, "%Y-%m-%dT%H:%M:%S")
        end_timestamp_datetime = datetime.datetime.strptime(
            end_timestamp, "%Y-%m-%dT%H:%M:%S")

        results = session.query(EquipmentStatusEvents).filter(and_(EquipmentStatusEvents.date_created >=
                                                                   start_timestamp_datetime,
                                                                   EquipmentStatusEvents.date_created <
                                                                   end_timestamp_datetime))

        results_list = []

        for event in results:
            results_list.append(event.to_dict())

        session.close()

        logger.info("Query for Equipment Status readings after %s returns %d results" % (
            start_timestamp, len(results_list)))

        return results_list, 200
    except Exception as e:
        logger.error(e)


def process_messages():
    """ Process event messages """
    hostname = "%s:%d" % (
        app_config["events"]["hostname"], app_config["events"]["port"])

    max_retries = app_config["startup"]["max_retries"]
    retry_count = 0

    while retry_count < max_retries:
        try:
            client = KafkaClient(hosts=hostname)
            topic = client.topics[str.encode(app_config["events"]["topic"])]
            logger.info("Connected Successfully")

            event_log_topic = client.topics[str.encode(app_config["events"]["topic2"])]
            event_log_producer = event_log_topic.get_sync_producer()

            startup_msg = {
                "type": "storage_startup",
                "datetime": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
                "payload": {
                    "message": "Storage service has started and connected to Kafka",
                    "code": "0002"
                }
            }
            event_log_producer.produce(json.dumps(startup_msg).encode('utf-8'))
            logger.info("Startup message published to event_log topic")

            break
        except Exception as e:
            logger.error(f"Connection Failed: {e}")
            time.sleep(app_config["startup"]["sleep"])
            retry_count += 1

    # Create a consume on a consumer group, that only reads new messages
    # (uncommitted messages) when the service re-starts (i.e., it doesn't
    # read all the old messages from the history in the message queue).
    consumer = topic.get_simple_consumer(consumer_group=b'event_group',
                                         reset_offset_on_start=False,
                                         auto_offset_reset=OffsetType.LATEST)

    # This is blocking - it will wait for a new message
    for msg in consumer:
        msg_str = msg.value.decode('utf-8')
        msg = json.loads(msg_str)
        logger.info(f"Message: {msg}")

        payload = msg["payload"]

        if msg["type"] == "usage_data_events":  # Change this to your event type
            # Store the event1 (i.e., the payload) to the DB
            session = DB_SESSION()
            logger.info("Storing Usage Data Event")

            usage_data = UsageDataEvents(uuid=payload["uuid"],
                                         shuttle_speed=payload["shuttle_speed"],
                                         angle=payload["angle"],
                                         usage_frequency=payload["usage_frequency"],
                                         trace_id=payload["trace_id"])

            session.add(usage_data)
            session.commit()
            session.close()
            logger.debug(
                f'Stored event usage_data_events request with a trace id of {payload["trace_id"]}')

        elif msg["type"] == "equipment_status_events":  # Change this to your event type
            # Store the event2 (i.e., the payload) to the DB
            session = DB_SESSION()
            logger.info("Storing Equipment Status Event")

            equipment_data = EquipmentStatusEvents(uuid=payload["uuid"],
                                                   wear_level=payload["wear_level"],
                                                   damage_status=payload["damage_status"],
                                                   last_strung_date=payload["last_strung_date"],
                                                   trace_id=payload["trace_id"])

            session.add(equipment_data)
            session.commit()
            session.close()
            logger.debug(
                f"""Stored event equipment_status_events 
                request with a trace id of {payload["trace_id"]}""")

        # Commit the new message as being read
        consumer.commit_offsets()
        logger.info("Event has been processed")
    logger.info("Processing has finished")


app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yml",
            base_path="/storage",
            strict_validation=True,
            validate_responses=True)

if __name__ == "__main__":
    t1 = Thread(target=process_messages)
    t1.setDaemon(True)
    t1.start()
    app.run(port=8090)
