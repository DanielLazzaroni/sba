import mysql.connector

db_conn = mysql.connector.connect(
    host="lazzaroni-cloud.eastus.cloudapp.azure.com",
    user="user",
    password="password",
    database="events"
)

db_cursor = db_conn.cursor()

db_cursor.execute('''
          CREATE TABLE usage_data_events
          (id INT NOT NULL AUTO_INCREMENT,
           uuid CHAR(36) NOT NULL,
           shuttle_speed INTEGER NOT NULL,
           angle INTEGER NOT NULL,
           usage_frequency INTEGER NOT NULL,
           trace_id CHAR(36) NOT NULL,
           date_created VARCHAR(100) NOT NULL,
           CONSTRAINT usage_data_events PRIMARY KEY (id))
          ''')

db_cursor.execute('''
          CREATE TABLE equipment_status_events
          (id INT NOT NULL AUTO_INCREMENT,
           uuid CHAR(36) NOT NULL,
           wear_level INTEGER NOT NULL,
           damage_status VARCHAR(100) NOT NULL,
           last_strung_date CHAR(100) NOT NULL,
           trace_id CHAR(36) NOT NULL,
           date_created VARCHAR(100) NOT NULL,
           CONSTRAINT equipment_status_events PRIMARY KEY (id))
          ''')

db_conn.commit()
db_conn.close()
