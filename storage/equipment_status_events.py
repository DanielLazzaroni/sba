import datetime
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

class EquipmentStatusEvents(Base):
    """ Equipment Status Events """

    __tablename__ = "equipment_status_events"

    id = Column(Integer, primary_key=True)
    uuid = Column(String(36), nullable=False)
    wear_level = Column(Integer, nullable=False)
    damage_status = Column(String(100), nullable=False)
    last_strung_date = Column(String(100), nullable=False)
    trace_id = Column(String(36), nullable=False)
    date_created = Column(DateTime, nullable=False)

    def __init__(self, uuid, wear_level, damage_status, last_strung_date, trace_id):
        """ Initializes equipment status report"""
        self.uuid = uuid
        self.wear_level = wear_level
        self.damage_status = damage_status
        self.last_strung_date = last_strung_date
        self.trace_id = trace_id
        self.date_created = datetime.datetime.now()

    def to_dict(self):
        """ Dictionary Representation of a equipment status report"""
        dict = {}
        dict['uuid'] = self.uuid
        dict['wear_level'] = self.wear_level
        dict['damage_status'] = self.damage_status
        dict['last_strung_date'] = self.last_strung_date
        dict['trace_id'] = self.trace_id
        dict['date_created'] = self.date_created

        return dict
