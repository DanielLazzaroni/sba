import datetime
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

class UsageDataEvents(Base):
    """ Usage Data Events """

    __tablename__ = "usage_data_events"

    id = Column(Integer, primary_key=True)
    uuid = Column(String(36), nullable=False)
    shuttle_speed = Column(Integer, nullable=False)
    angle = Column(Integer, nullable=False)
    usage_frequency = Column(Integer, nullable=False)
    trace_id = Column(String(36), nullable=False)
    date_created = Column(DateTime, nullable=False)

    def __init__(self, uuid, shuttle_speed, angle, usage_frequency, trace_id):
        """ Initializes an usage data report"""
        self.uuid = uuid
        self.shuttle_speed = shuttle_speed
        self.angle = angle
        self.usage_frequency = usage_frequency
        self.trace_id = trace_id
        self.date_created = datetime.datetime.now()

    def to_dict(self):
        """ Dictionary Representation of an usage data report"""
        dict = {}
        dict['uuid'] = self.uuid
        dict['shuttle_speed'] = self.shuttle_speed
        dict['angle'] = self.angle
        dict['usage_frequency'] = self.usage_frequency
        dict['trace_id'] = self.trace_id
        dict['date_created'] = self.date_created

        return dict
