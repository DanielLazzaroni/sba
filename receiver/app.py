import logging
import logging.config
from uuid import uuid4 as uuid
from datetime import datetime as dt
import json
import time
import os
import yaml
from pykafka import KafkaClient
import connexion
from connexion import NoContent
#wedfg
if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    APP_CONF_FILE = "/config/app_conf.yml"
    LOG_CONF_FILE = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    APP_CONF_FILE = "app_conf.yml"
    LOG_CONF_FILE = "log_conf.yml"

with open(APP_CONF_FILE, 'r') as f:
    app_config = yaml.safe_load(f.read())

# External Logging Configuration
with open(LOG_CONF_FILE, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info(f'App Conf File: {APP_CONF_FILE}')
logger.info(f'Log Conf File: {LOG_CONF_FILE}')


max_retries = app_config["startup"]["max_retries"]
retry_count = 0

while retry_count < max_retries:
    try:
        kafka_host = f'{app_config["events"]["hostname"]}:{app_config["events"]["port"]}'
        client = KafkaClient(hosts=kafka_host)

        topic = client.topics[str.encode(app_config["events"]["topic"])]
        producer = topic.get_sync_producer()

        event_log_topic = client.topics[str.encode(app_config["events"]["topic2"])]
        event_log_producer = event_log_topic.get_sync_producer()

        logger.info("Connected Successfully")

        startup_msg = {
            "type": "receiver_startup",
            "datetime": dt.now().strftime("%Y-%m-%dT%H:%M:%S"),
            "payload": {
                "message": "Receiver service has started and connected to Kafka",
                "code": "0001"
            }
        }
        event_log_producer.produce(json.dumps(startup_msg).encode('utf-8'))
        logger.info("Startup message published to event_log topic")

        break
    except Exception as e:
        logger.error(f"Connection Failed: {e}")
        time.sleep(app_config["startup"]["sleep"])
        retry_count += 1

def usage_data_events(body):
    trace_id = str(uuid())
    logger.info(f'Received event usage_data_events request with a trace id of {trace_id}')

    body['trace_id'] = trace_id

    try:
        msg = { "type": "usage_data_events",
            "datetime" : dt.now().strftime("%Y-%m-%dT%H:%M:%S"),
            "payload": body}
    except Exception as e:
        logger.info(e)

    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))

    logger.info(f'Returned event usage_data_events response (Id: {trace_id}) with status 201')

    return NoContent, 201

def equipment_status_events(body):
    trace_id = str(uuid())
    logger.info(f'Received event equipment_status_events request with a trace id of {trace_id}')

    body['trace_id'] = trace_id

    msg = {"type": "equipment_status_events",
           "datetime" : dt.now().strftime("%Y-%m-%dT%H:%M:%S"),
           "payload": body }

    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))

    logger.info(f'Returned event equipment_status_events response (Id: {trace_id}) with status 201')

    return NoContent, 201

app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yml",
            base_path="/receiver",
            strict_validation=True,
            validate_responses=True)

if __name__ == "__main__":
    app.run(port=8080)
